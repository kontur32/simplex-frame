module namespace config = "simplex-frame/v0.1/config";

declare
  %public
function config:param($param as xs:string) as xs:string* {
  let $confPath := config:param("config.xml", "app-config")
  return
    config:param($confPath, $param)
};

declare
  %private
function config:param($path as xs:string, $param as xs:string) as xs:string* {
  config:file($path)/param[@id=$param]/text()
};

declare
  %private
function config:file($path as xs:string) as element(config)* {
  doc($path)/config
};