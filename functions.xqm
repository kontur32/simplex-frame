module namespace funct = "simplex-frame/v0.1/funct";

import module namespace config = "simplex-frame/v0.1/config" at "config.xqm";

declare function funct:replace($string, $map){
  fold-left(
    map:for-each($map, function($key, $value){map{$key:$value}}),
    $string, 
    function($string, $d){
       replace(
        $string,
        "\{\{" || map:keys($d)[1] || "\}\}",
        replace(
          serialize(map:get($d, map:keys($d)[1])), '\\', '\\\\'
        ) (: проблема \ в заменяемой строке :)
      ) 
    }
  )
};

declare
  %private
function funct:xhtml($componentsPath as xs:string, $map as map(*))
  as element()*
{
  let $string := file:read-text($componentsPath || ".html") 
  return
    parse-xml(funct:replace($string, $map))/child::*
};

declare function funct:tpl($app, $params){
  let $rootPath := '../app/components'
  let $componentPath :=  $rootPath || "/" || $app || "/" || tokenize($app, "/")[last()]
  
  let $_tpl := function($app, $params){funct:tpl($app, $params)}
  let $_config := function($param){config:param($param)}
  let $_lib :=
    function($moduleName, $functName, $params){funct:lib($moduleName, $functName, $params)}
  
  return
    if(
      file:exists(file:base-dir() || $componentPath|| ".xqm") and
      file:exists(file:base-dir() || $componentPath|| ".html") 
    )
    then(
       let $paramsFunct :=
         map:merge( 
            ($params, map{'_' : $_tpl, '_config' : $_config, '_lib': $_lib})
          )
        let $result := funct:call($componentPath, $paramsFunct)  
        return
           funct:xhtml(file:base-dir() || $componentPath, $result)
    )
    else("Error: component " || $componentPath || " not exists")
};

(:
  вызывает основную фукнцию компонента main
:)
declare function funct:call($modulePath, $params){
  funct:call($modulePath, "main", $params)
};

(:
  вызывает функцию из клиентской библиотеки
:)
declare function funct:lib($moduleName, $functName, $params){
  for $i in inspect:functions('../app/lib/' || $moduleName || ".xqm")
  where local-name-from-QName(function-name($i)) = 'main'
  return
       $i($functName, $params)
};

(:
  вызывает  фукнцию модуля
:)
declare function funct:call($modulePath, $functName, $params){
  for $i in inspect:functions($modulePath || ".xqm")
  where local-name-from-QName(function-name($i)) = $functName
  return
       $i($params)
};